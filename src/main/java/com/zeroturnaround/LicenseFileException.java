package com.zeroturnaround;

public class LicenseFileException extends Exception
{
  public LicenseFileException(String str)
  {
    super(str);
  }

  public LicenseFileException(String str, Throwable e) {
    super(str, e);
  }

  public LicenseFileException()
  {
  }
}