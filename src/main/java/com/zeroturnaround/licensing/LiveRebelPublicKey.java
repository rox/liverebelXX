package com.zeroturnaround.licensing;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import play.Logger;

public class LiveRebelPublicKey
{
  private static final byte[] PUB_KEY_BYTES ={ 48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127, -115, 0, 48, -127, -119, 2, -127, -127, 0, -100, -40, 15, 116, -48, -61, 119, -84, 59, 94, -56, 43, 62, -11, 103, 29, 55, 87, -72, -40, -42, -81, -14, 79, 33, 87, -68, -118, -86, -113, 34, 79, -25, -14, 13, 119, 3, 18, -108, 35, -44, 58, 114, 10, -110, -94, -101, -122, 81, 2, -6, -30, -117, 37, -123, 89, -67, 98, -50, -80, -76, 56, 13, 42, -38, -42, 43, -55, 61, 60, -24, 120, 57, -38, -16, 125, -101, 59, 101, 17, -109, 74, -52, -126, -34, 99, 116, -98, 80, 34, 8, 3, -124, 73, 27, -120, -84, 73, 2, -13, -122, -24, 119, 68, 118, 56, 54, 96, 63, -103, -109, -108, -57, 3, -60, -40, 39, 34, 80, -103, 91, 48, -87, 69, 83, -59, 23, -57, 2, 3, 1, 0, 1 };

  private static BouncyCastleProvider bcp = new BouncyCastleProvider();

  public static final PublicKey getPublicKey()
  {
    X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(PUB_KEY_BYTES);
    try
    {
      return KeyFactory.getInstance("RSA", bcp).generatePublic(pubSpec);
    } catch (NoSuchAlgorithmException e) {
      Logger.debug(e, "RSA algorithm doesn't exist? What?", new Object[0]);
    } catch (InvalidKeySpecException e) {
      Logger.error(e, "Client License Public key seems to be corrupted!", new Object[0]);
    }
    return null;
  }
}