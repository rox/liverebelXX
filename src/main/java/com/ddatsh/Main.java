package com.ddatsh;

import java.io.File;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.zeroturnaround.licensing.UserLicense;

public class Main {

	public static void main(String[] args) throws Exception {
		BouncyCastleProvider bcp = new BouncyCastleProvider();
		X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(StringUtils.pk);
		PublicKey publicKey = KeyFactory.getInstance("RSA", bcp).generatePublic(pubSpec);
		UserLicense lic = UserLicense.loadInstance(new File("d:/test/liverebel.lic"), publicKey);
		WriteLic.readLic(lic);
		System.out.println(lic.selfVerify(publicKey));
	}

}
