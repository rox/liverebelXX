package com.ddatsh;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.zeroturnaround.licensing.UserLicense;

public class WriteLic {
	
	public static String originLicPath = "d:/server/liverebel/liverebel.lic";
	public static String newLicpath = "d:/test/liverebel.lic";

	public static UserLicense loadLic(String licPath) throws Exception {
		UserLicense lic = null;
		ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(licPath)));
		Object obj = ois.readObject();
		ois.close();
		if ((obj != null) && (obj instanceof UserLicense)) {
			lic = (UserLicense) obj;
		}
		return lic;
	}

	public static void readLic(UserLicense lic) throws Exception {
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(lic.getLicense()));
		Map localMap = (Map) ois.readObject();
		Iterator it = localMap.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next().toString();
			System.out.println(key + "= " + localMap.get(key));
		}
		ois.close();
	}

	public static void modifyLic(String newLicPath, Map map, UserLicense originLic) throws Exception {

		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(originLic.getLicense()));
		Map localMap = (Map) ois.readObject();
		localMap.putAll(map);

		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bao);
		oos.writeObject(localMap);
		oos.close();
		originLic.setLicense(bao.toByteArray());
		originLic.setSignature(StringUtils.gs(originLic.getLicense()));
		oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(newLicPath)));

		oos.writeObject(originLic);
		oos.close();

	}

	public static void verifyWithOriginPubKey(UserLicense lic) {
		System.out.println("verifyWithOriginPubKey result :" + StringUtils.vsOrigin(lic.getLicense(), lic.getSignature()));
	}

	public static void verify(UserLicense lic) {
		System.out.println("verify result :" + StringUtils.vs(lic.getLicense(), lic.getSignature()));
	}

	public static void main(String[] args) throws Exception {

		UserLicense originLic = loadLic(originLicPath);
		verifyWithOriginPubKey(originLic);
		readLic(originLic);
		////////////////
		Map localMap = new HashMap();
		localMap.put("commercial", "true");
		localMap.put("Seats", "99999999");
		localMap.put("Type", "production");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		Date validUntil = sdf.parse("2999-12-31");
		localMap.put("limitedFrom", new Date());
		localMap.put("limitedUntil", validUntil);

		///////////////

		modifyLic(newLicpath, localMap, originLic);
		System.out.println();
		System.out.println("###############");
		System.out.println();
		UserLicense newLic = loadLic(newLicpath);
		readLic(newLic);
		System.out.println();
		verifyWithOriginPubKey(newLic);
		verify(newLic);

	}

}
