package com.ddatsh;


import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.zeroturnaround.licensing.LiveRebelPublicKey;

public class StringUtils {
	public static final byte[] prk =  { 48, -126, 2, 118, 2, 1, 0, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 4, -126, 2, 96, 48, -126, 2, 92, 2, 1, 0, 2, -127, -127, 0, -100, -40, 15, 116, -48, -61, 119, -84, 59, 94, -56, 43, 62, -11, 103, 29, 55, 87, -72, -40, -42, -81, -14, 79, 33, 87, -68, -118, -86, -113, 34, 79, -25, -14, 13, 119, 3, 18, -108, 35, -44, 58, 114, 10, -110, -94, -101, -122, 81, 2, -6, -30, -117, 37, -123, 89, -67, 98, -50, -80, -76, 56, 13, 42, -38, -42, 43, -55, 61, 60, -24, 120, 57, -38, -16, 125, -101, 59, 101, 17, -109, 74, -52, -126, -34, 99, 116, -98, 80, 34, 8, 3, -124, 73, 27, -120, -84, 73, 2, -13, -122, -24, 119, 68, 118, 56, 54, 96, 63, -103, -109, -108, -57, 3, -60, -40, 39, 34, 80, -103, 91, 48, -87, 69, 83, -59, 23, -57, 2, 3, 1, 0, 1, 2, -127, -128, 88, 22, 68, 127, -1, -21, 119, -33, -60, 44, 84, 81, 109, 119, -105, -28, -31, -69, 35, -74, -84, -60, 126, 91, 16, -57, -36, -67, 40, 83, 97, 26, -19, -110, -107, 27, 93, 31, 103, 17, -60, 84, -38, -51, -77, 83, 29, -13, -50, -75, -44, -65, -67, 20, 98, -109, 102, -7, 2, 74, 93, -67, 9, 47, 63, -70, 4, 39, 53, 16, -79, 100, -119, 1, 51, -36, 101, -14, -27, -84, 120, 13, -16, -42, -89, 50, -21, -81, 77, 93, -61, 49, 55, 91, 2, 126, -58, 14, -84, -121, -103, -8, -68, 121, -114, 61, -9, 82, 63, 106, -76, 107, 26, -114, -98, -10, 32, 46, -119, 84, 45, -65, -89, 91, -64, -128, -39, 105, 2, 65, 0, -24, -2, 25, 45, -119, 59, 38, 96, -45, -60, 114, -95, 24, -83, -107, -93, -40, -85, 91, 9, -74, 51, 15, 3, -80, 5, -94, 1, 24, -9, 82, 10, 39, -123, 50, 43, -54, 27, 55, -18, -97, 73, 13, 49, 65, -54, -97, 124, -62, -52, 102, -30, 113, -119, 107, -13, -76, 14, 97, 0, -38, 29, -108, 67, 2, 65, 0, -84, 84, -7, -104, -124, 34, -118, -78, -40, 62, -49, -42, -114, -62, 29, -79, 100, 28, 120, 124, -89, -70, -64, 33, -98, 88, 7, 73, 104, 57, 12, 105, 100, 79, 15, -66, 100, -89, -6, 116, -72, -2, -45, -73, 114, -126, -112, -47, 15, 72, -82, -114, -13, -57, 112, -44, -62, -120, 23, 124, -29, -55, 88, 45, 2, 65, 0, -126, 127, -121, -44, -92, 111, 119, -32, 117, -36, 43, 36, 37, -15, 91, -58, 8, -98, 121, -128, 40, 7, -45, 26, 10, -90, 45, 6, 22, 88, -72, 31, -83, -31, 1, -89, -37, 44, -127, -104, 106, -88, -4, -11, -96, -42, -1, -101, 94, 116, 36, 16, 34, -19, -120, -124, 88, 78, 81, 112, -85, -70, 51, -121, 2, 64, 50, 114, -103, -78, 117, 19, -21, -120, 89, 87, 97, -34, 76, 98, 52, -15, 5, -2, 115, -93, 2, 50, -32, 37, -73, 69, -53, -76, 72, -121, 82, -61, -45, 114, 117, -120, 101, -52, -22, -69, -102, -96, 54, 120, 32, 84, 106, 114, 88, -56, 13, 25, -103, -15, -117, 109, 45, 25, 75, -50, -22, -101, -71, 109, 2, 64, 60, 9, 2, -10, -61, -41, -88, 62, 82, -110, -12, -11, -70, 33, -5, -47, -73, 70, -70, 24, 71, -5, -77, -44, -63, -97, 36, -5, 47, 14, 72, -90, 74, 54, 12, -80, -48, -74, -37, -95, 56, 4, 9, 93, -57, 100, 103, 79, -75, -62, -65, 35, 102, -83, -13, 1, 31, 3, 108, 41, -69, 23, -106, -100 };

	public static final byte[] pk = { 48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127, -115, 0, 48, -127, -119, 2, -127, -127, 0, -100, -40, 15, 116, -48, -61, 119, -84, 59, 94, -56, 43, 62, -11, 103, 29, 55, 87, -72, -40, -42, -81, -14, 79, 33, 87, -68, -118, -86, -113, 34, 79, -25, -14, 13, 119, 3, 18, -108, 35, -44, 58, 114, 10, -110, -94, -101, -122, 81, 2, -6, -30, -117, 37, -123, 89, -67, 98, -50, -80, -76, 56, 13, 42, -38, -42, 43, -55, 61, 60, -24, 120, 57, -38, -16, 125, -101, 59, 101, 17, -109, 74, -52, -126, -34, 99, 116, -98, 80, 34, 8, 3, -124, 73, 27, -120, -84, 73, 2, -13, -122, -24, 119, 68, 118, 56, 54, 96, 63, -103, -109, -108, -57, 3, -60, -40, 39, 34, 80, -103, 91, 48, -87, 69, 83, -59, 23, -57, 2, 3, 1, 0, 1 };

	public static final byte[] gs(byte[] data) {
		byte[] res = null;
		try {
			PKCS8EncodedKeySpec priKeySpec = new PKCS8EncodedKeySpec(prk);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey priKey = keyFactory.generatePrivate(priKeySpec);
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initSign(priKey);
			sig.update(data);
			res = sig.sign();
		} catch (SignatureException ex) {
			ex.printStackTrace();
		} catch (InvalidKeyException ex) {
			ex.printStackTrace();
		} catch (InvalidKeySpecException ex) {
			ex.printStackTrace();
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}
		return res;
	}

	public static final boolean vsOrigin(byte[] data, byte[] signature) {
		boolean flag = false;
		try {
			PublicKey pubKey =LiveRebelPublicKey.getPublicKey();
			
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(pubKey);
			sig.update(data);
			flag = sig.verify(signature);
		 
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flag;
	}
	
	public static final boolean vs(byte[] data, byte[] signature) {
		boolean flag = false;
		try {
			X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pk);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(pubKey);
			sig.update(data);
			flag = sig.verify(signature);
		 
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flag;
	}

	public static final void generateKeyBytesArray() {
		KeyPairGenerator keygen;
		try {
			keygen = KeyPairGenerator.getInstance("RSA");
			
			keygen.initialize(2048);
			KeyPair keys = keygen.genKeyPair();
		 
			System.out
					.println(byteArrayToString(keys.getPrivate().getEncoded()));
			System.out
					.println("################################################");
			System.out
					.println(byteArrayToString(keys.getPublic().getEncoded()));
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}
	}
	
	public static final void generateBCKeyBytesArray() {
		KeyPairGenerator keygen;
		try {
			BouncyCastleProvider bcp = new BouncyCastleProvider();
			keygen = KeyPairGenerator.getInstance("RSA",bcp);
			
			keygen.initialize(1024);
			KeyPair keys = keygen.genKeyPair();
		 
			System.out
					.println(byteArrayToString(keys.getPrivate().getEncoded()));
			System.out
					.println("################################################");
			System.out
					.println(byteArrayToString(keys.getPublic().getEncoded()));
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}
	}

	public static final String byteArrayToString(byte[] ba) {
		String ret = "private static final byte[] a = { ";
		for (int i = 0; i < ba.length - 1; i++) {
			ret = ret + ba[i] + ", ";
		}
		ret = ret + ba[ba.length - 1] + " };";
		return ret;
	}

	public static void main(String[] args) {
		generateKeyBytesArray();
		generateBCKeyBytesArray();
	}
}